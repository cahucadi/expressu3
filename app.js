var express = require('express');
var bodyParser = require('body-parser');
var router = require('./routers/router');

var app = express();
app.use( express.json() );
app.use( express.urlencoded( { extended: true }) );

// Routes
app.use( router );

app.use( ( req, res, next )  => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods','GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
} );

module.exports = app;